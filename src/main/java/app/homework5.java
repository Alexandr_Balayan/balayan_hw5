package app;

import java.util.Scanner;

public class homework5 {
    public static void main(String[] args) {
        System.out.println("Введите пример:");
        Scanner input = new Scanner(System.in);
        String task = input.nextLine().replaceAll(" ", "");
        if (task.matches("[0-9]*[/*+-][0-9]*")) {
            String[] values = task.split("[/*+-]");
            double firstValue = Double.parseDouble(values[0]);
            double secondValue = Double.parseDouble(values[1]);
            if (task.contains("/")) {
                if (secondValue == 0) {
                    System.out.println("Делить на ноль нельзя (по крайней мере тут)");
                } else {
                    System.out.println(firstValue / secondValue);
                }
            } else if (task.contains("*")) {
                System.out.println(firstValue * secondValue);
            } else if (task.contains("+")) {
                System.out.println(firstValue + secondValue);
            } else if (task.contains("-")) {
                System.out.println(firstValue - secondValue);
            }
        } else {
            System.out.println("Введено некорректное сообщение, я не умею такое считать.");
        }
    }
}